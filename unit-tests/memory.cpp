//
// Created by Andy Lin on 20.05.21.
//

#include "gtest/gtest.h"
#include "../src/common/exceptions/MemoryException.h"
#include "../src/common/game_state/player/Player.h"
#include "../src/common/game_state/cards/Card.h"
#include "../src/common/game_state/cards/CardBoard.h"
#include "../src/common/serialization/json_utils.h"


/* A test fixture allows to reuse the same configuration of objects for all
 * tests in a test suite. The name of the fixture must match the test suite.
 *
 * For each test defined with TEST_F(), googletest will create a fresh test
 * fixture at runtime, immediately initialize it via SetUp(), run the test,
 * clean up by calling TearDown(), and then delete the test fixture.
 * Note that different tests in the same test suite have different test fixture
 * objects, and googletest always deletes a test fixture before it creates the
 * next one. googletest does not reuse the same test fixture for multiple
 * tests. Any changes one test makes to the fixture do not affect other tests.
 */
class MemoryTest : public ::testing::Test {

protected:
    virtual void SetUp() {
        players.resize(2);
        players[0].push_back(new Player("id1", "player1", 0));
        players[1].push_back(new Player("id2", "player2", 0));
        cb.setup_game(err);
        cards = cb.getCards();
    }

    /* Any object and subroutine declared here can be accessed in the tests */

    // cards[i][j] holds a pointer to the j-th copy of a card of value i
    std::vector<Card*> cards;
    std::vector<std::vector<Player*>> players;
    CardBoard cb;
    std::string err;
};

// Choosing a card by a player must succeed.
TEST_F(MemoryTest, ChooseOneCard) {
    EXPECT_EQ(players[0][0]->get_id(), "id1");
    ASSERT_EQ(cards[1]->getIsFront(), 0);
    cards[1]->flip();
    EXPECT_EQ(cards[1]->getIsFront(), 1);
}

// The initial state must be that the cardboard is full of unflipped cards.
TEST_F(MemoryTest, CardboardCheck) {
    ASSERT_EQ(cb.getAvailableCards(), 12);

    // Cards not yet turned will be counted into the getNofTurnedCards() function.
    ASSERT_EQ(cb.getAvailableCards() - cb.getNofTurnedCards(), 0);
}

// Choosing two cards that form a pair by a player must succeed.
TEST_F(MemoryTest, CardPairTest) {
    EXPECT_EQ(players[0][0]->get_id(), "id1");

    // If two cards form a pair, they should be vanishable.
    EXPECT_TRUE(cb.isVanishable(0, 2, 1, 2));
    EXPECT_TRUE(cb.isVanishable(0, 2, 1, 0));
    EXPECT_TRUE(cb.isVanishable(0, 2, 0, 3));
    EXPECT_TRUE(cb.isVanishable(1, 2, 1, 0));
    EXPECT_TRUE(cb.isVanishable(1, 2, 0, 3));
    EXPECT_TRUE(cb.isVanishable(1, 0, 0, 3));

    EXPECT_TRUE(cb.isVanishable(1, 3, 2, 3));
    EXPECT_TRUE(cb.isVanishable(1, 3, 2, 1));
    EXPECT_TRUE(cb.isVanishable(1, 3, 0, 1));
    EXPECT_TRUE(cb.isVanishable(2, 3, 2, 1));
    EXPECT_TRUE(cb.isVanishable(2, 3, 0, 1));
    EXPECT_TRUE(cb.isVanishable(2, 1, 0, 1));

    EXPECT_TRUE(cb.isVanishable(2, 2, 0, 0));
    EXPECT_TRUE(cb.isVanishable(2, 2, 1, 1));
    EXPECT_TRUE(cb.isVanishable(2, 2, 2, 0));
    EXPECT_TRUE(cb.isVanishable(0, 0, 1, 1));
    EXPECT_TRUE(cb.isVanishable(0, 0, 2, 0));
    EXPECT_TRUE(cb.isVanishable(1, 1, 2, 0));
}

// Choosing two cards that do not form a pair by a player must fail.
TEST_F(MemoryTest, CardNotPairedTest) {
    EXPECT_EQ(players[0][0]->get_id(), "id1");
    EXPECT_FALSE(cb.isVanishable(0, 2, 1, 3));
    EXPECT_FALSE(cb.isVanishable(0, 2, 2, 3));
    EXPECT_FALSE(cb.isVanishable(0, 2, 2, 1));
    EXPECT_FALSE(cb.isVanishable(0, 2, 0, 1));
    EXPECT_FALSE(cb.isVanishable(0, 2, 2, 2));
    EXPECT_FALSE(cb.isVanishable(0, 2, 0, 0));
    EXPECT_FALSE(cb.isVanishable(0, 2, 1, 1));
    EXPECT_FALSE(cb.isVanishable(0, 2, 2, 0));

    EXPECT_FALSE(cb.isVanishable(1, 2, 1, 3));
    EXPECT_FALSE(cb.isVanishable(1, 2, 2, 3));
    EXPECT_FALSE(cb.isVanishable(1, 2, 2, 1));
    EXPECT_FALSE(cb.isVanishable(1, 2, 0, 1));
    EXPECT_FALSE(cb.isVanishable(1, 2, 2, 2));
    EXPECT_FALSE(cb.isVanishable(1, 2, 0, 0));
    EXPECT_FALSE(cb.isVanishable(1, 2, 1, 1));
    EXPECT_FALSE(cb.isVanishable(1, 2, 2, 0));

    EXPECT_FALSE(cb.isVanishable(1, 0, 1, 3));
    EXPECT_FALSE(cb.isVanishable(1, 0, 2, 3));
    EXPECT_FALSE(cb.isVanishable(1, 0, 2, 1));
    EXPECT_FALSE(cb.isVanishable(1, 0, 0, 1));
    EXPECT_FALSE(cb.isVanishable(1, 0, 2, 2));
    EXPECT_FALSE(cb.isVanishable(1, 0, 0, 0));
    EXPECT_FALSE(cb.isVanishable(1, 0, 1, 1));
    EXPECT_FALSE(cb.isVanishable(1, 0, 2, 0));

    EXPECT_FALSE(cb.isVanishable(0, 3, 1, 3));
    EXPECT_FALSE(cb.isVanishable(0, 3, 2, 3));
    EXPECT_FALSE(cb.isVanishable(0, 3, 2, 1));
    EXPECT_FALSE(cb.isVanishable(0, 3, 0, 1));
    EXPECT_FALSE(cb.isVanishable(0, 3, 2, 2));
    EXPECT_FALSE(cb.isVanishable(0, 3, 0, 0));
    EXPECT_FALSE(cb.isVanishable(0, 3, 1, 1));
    EXPECT_FALSE(cb.isVanishable(0, 3, 2, 0));

    EXPECT_FALSE(cb.isVanishable(1, 3, 2, 2));
    EXPECT_FALSE(cb.isVanishable(1, 3, 0, 0));
    EXPECT_FALSE(cb.isVanishable(1, 3, 1, 1));
    EXPECT_FALSE(cb.isVanishable(1, 3, 2, 0));

    EXPECT_FALSE(cb.isVanishable(2, 3, 2, 2));
    EXPECT_FALSE(cb.isVanishable(2, 3, 0, 0));
    EXPECT_FALSE(cb.isVanishable(2, 3, 1, 1));
    EXPECT_FALSE(cb.isVanishable(2, 3, 2, 0));

    EXPECT_FALSE(cb.isVanishable(2, 1, 2, 2));
    EXPECT_FALSE(cb.isVanishable(2, 1, 0, 0));
    EXPECT_FALSE(cb.isVanishable(2, 1, 1, 1));
    EXPECT_FALSE(cb.isVanishable(2, 1, 2, 0));

    EXPECT_FALSE(cb.isVanishable(0, 1, 2, 2));
    EXPECT_FALSE(cb.isVanishable(0, 1, 0, 0));
    EXPECT_FALSE(cb.isVanishable(0, 1, 1, 1));
    EXPECT_FALSE(cb.isVanishable(0, 1, 2, 0));
}

// Choosing an already flipped-front card by a player must fail.
TEST_F(MemoryTest, CardFlipTest) {
    EXPECT_EQ(players[1][0]->get_id(), "id2");

    // Set up the environment that a card has been flipped.
    cb.flipCard(1, 0);

    // Only unflipped cards can be flipped. Hence if a card has been fliiped, its getIsFront() value should be 1. That indicates the card cannot be flipped.
    // getIsFront() xan also be treated as a flip indicator. "0" = A card can be flipped. "1" = A card cannot be flipped.
    ASSERT_NE(cards[6]->getIsFront(), 0);
}

// Each succesful chosen pair of cards must increase the score of the player who selected them by 2.
TEST_F(MemoryTest, AddPlayerScore) {
    EXPECT_EQ(players[0][0]->get_id(), "id1");

    // A new test should start when a player just begins the game. That is, the score is at 0.
    EXPECT_EQ(players[0][0]->get_score(), 0);

    ASSERT_TRUE(cb.flipCard(1, 0));
    ASSERT_EQ(cards[6]->getIsFront(), 1);
    ASSERT_TRUE(cb.flipCard(1, 2));
    ASSERT_EQ(cards[3]->getIsFront(), 1);
    EXPECT_TRUE(cb.isVanishable(1, 0, 1, 2));

    // Add score for the player as two cards form a pair and these two cards have passed the test.
    players[0][0]->set_score(players[0][0]->get_score() + 2);

    EXPECT_EQ(players[0][0]->get_score(), 2);
}

// Each shuffle executed by a must succeed.
TEST_F(MemoryTest, InvokeShuffle) {
    EXPECT_EQ(players[0][0]->get_id(), "id1");
    cb.shuffle();

    // Validate if cards are being shuffled.
    int initial_value = 1;
    int total_different_values = 0;
    for (int i = 0; i < 12; i++) {
        if (cards[i]->getValue() != initial_value) {
            total_different_values++;
        }
        initial_value++;
    }

    EXPECT_NE(total_different_values, 0);
}


// If two chosen cards form a pair, they must be vanished from the cardboard.
TEST_F(MemoryTest, VanishPair) {
    EXPECT_EQ(players[0][0]->get_id(), "id1");

    // 1. Pick a card and flip it front.
    // 2. Check if the card is at the front face.
    // 3. Pick another card and flip it front.
    // 4. Check if this specific card is at the front face.
    // 5. Verify that two cards are vanishable.
    // 6. Vanish the card pair.
    // 7. Check if the cards are vanished.

    ASSERT_TRUE(cb.flipCard(1, 0));
    ASSERT_EQ(cards[6]->getIsFront(), 1);
    ASSERT_TRUE(cb.flipCard(1, 2));
    ASSERT_EQ(cards[3]->getIsFront(), 1);
    EXPECT_TRUE(cb.isVanishable(1, 0, 1, 2));
    cb.vanishPairs(1, 0, 1, 2);
    EXPECT_FALSE(cb.isVanishable(1, 0, 1, 2));
}

// If two chosen cards do not form a pair, they must be all flipped to the back.
TEST_F(MemoryTest, NotVanishable) {
    EXPECT_EQ(players[0][0]->get_id(), "id1");

    // 1. Pick a card and flip it front.
    // 2. Check if the card is at the front face.
    // 3. Pick another card and flip it front.
    // 4. Check if this specific card is at the front face.
    // 5. Verify that two cards are not vanishable.
    // 6. Flip back two cards.
    // 7. Check that the two cards are now faced front.

    ASSERT_TRUE(cb.flipCard(1, 0));
    ASSERT_EQ(cards[6]->getIsFront(), 1);
    ASSERT_TRUE(cb.flipCard(1, 1));
    ASSERT_EQ(cards[8]->getIsFront(), 1);
    EXPECT_FALSE(cb.isVanishable(1, 0, 1, 1));
    cards[6]->flip();
    cards[8]->flip();
    ASSERT_EQ(cards[6]->getIsFront(), 0);
    ASSERT_EQ(cards[8]->getIsFront(), 0);
}

// Removing the final pair from the cardboard must succeed with no cards available on the cardboard.
TEST_F(MemoryTest, RemoveFinalPair) {
    EXPECT_EQ(players[0][0]->get_id(), "id1");

    // This time the check removes all card pairs to see if the all cards are actually being removed.
    // 1. Pick a card and flip it front.
    // 2. Check if the card is at the front face.
    // 3. Pick another card and flip it front.
    // 4. Check if this specific card is at the front face.
    // 5. Verify that two cards are vanishable.
    // 6. Vanish the card pair.
    // 7. Check if the cards are vanished.

    ASSERT_TRUE(cb.flipCard(1, 0));
    ASSERT_EQ(cards[6]->getIsFront(), 1);
    ASSERT_TRUE(cb.flipCard(1, 2));
    ASSERT_EQ(cards[3]->getIsFront(), 1);
    EXPECT_TRUE(cb.isVanishable(1, 0, 1, 2));
    cb.vanishPairs(1, 0, 1, 2);
    EXPECT_FALSE(cb.isVanishable(1, 0, 1, 2));

    ASSERT_TRUE(cb.flipCard(0, 2));
    ASSERT_EQ(cards[0]->getIsFront(), 1);
    ASSERT_TRUE(cb.flipCard(0, 3));
    ASSERT_EQ(cards[9]->getIsFront(), 1);
    EXPECT_TRUE(cb.isVanishable(0, 2, 0, 3));
    cb.vanishPairs(0, 2, 0, 3);
    EXPECT_FALSE(cb.isVanishable(0, 2, 0, 3));

    ASSERT_TRUE(cb.flipCard(1, 3));
    ASSERT_EQ(cards[1]->getIsFront(), 1);
    ASSERT_TRUE(cb.flipCard(2, 3));
    ASSERT_EQ(cards[4]->getIsFront(), 1);
    EXPECT_TRUE(cb.isVanishable(1, 3, 2, 3));
    cb.vanishPairs(1, 3, 2, 3);
    EXPECT_FALSE(cb.isVanishable(1, 3, 2, 3));

    ASSERT_TRUE(cb.flipCard(2, 1));
    ASSERT_EQ(cards[7]->getIsFront(), 1);
    ASSERT_TRUE(cb.flipCard(0, 1));
    ASSERT_EQ(cards[10]->getIsFront(), 1);
    EXPECT_TRUE(cb.isVanishable(2, 1, 0, 1));
    cb.vanishPairs(2, 1, 0, 1);
    EXPECT_FALSE(cb.isVanishable(2, 1, 0, 1));

    ASSERT_TRUE(cb.flipCard(2, 2));
    ASSERT_EQ(cards[2]->getIsFront(), 1);
    ASSERT_TRUE(cb.flipCard(0, 0));
    ASSERT_EQ(cards[5]->getIsFront(), 1);
    EXPECT_TRUE(cb.isVanishable(2, 2, 0, 0));
    cb.vanishPairs(2, 2, 0, 0);
    EXPECT_FALSE(cb.isVanishable(2, 2, 0, 0));

    ASSERT_TRUE(cb.flipCard(1, 1));
    ASSERT_EQ(cards[8]->getIsFront(), 1);
    ASSERT_TRUE(cb.flipCard(2, 0));
    ASSERT_EQ(cards[11]->getIsFront(), 1);
    EXPECT_TRUE(cb.isVanishable(1, 1, 2, 0));
    cb.vanishPairs(1, 1, 2, 0);
    EXPECT_FALSE(cb.isVanishable(1, 1, 2, 0));

    // The game should now end.
    EXPECT_TRUE(cb.processEndGame());
}

// The record that any flipped card by a player must succeed.
TEST_F(MemoryTest, CardFlipRecoard) {
    EXPECT_EQ(players[0][0]->get_id(), "id1");

    ASSERT_TRUE(cb.flipCard(1, 0));
    ASSERT_EQ(cards[6]->getIsFront(), 1);
    EXPECT_EQ(cb.getAvailableCards() - cb.getNofTurnedCards(), 1);
}

// Multiplayer playability must succeed.
TEST_F(MemoryTest, MultiPlayerCheck) {
    // Player 1 successfully vanishes a pair of cards. Then it is Player 2's turn.
    EXPECT_EQ(players[0][0]->get_id(), "id1");
    EXPECT_EQ(players[0][0]->get_score(), 0);
    ASSERT_TRUE(cb.flipCard(1, 0));
    ASSERT_EQ(cards[6]->getIsFront(), 1);
    ASSERT_TRUE(cb.flipCard(1, 2));
    ASSERT_EQ(cards[3]->getIsFront(), 1);
    EXPECT_TRUE(cb.isVanishable(1, 0, 1, 2));
    cb.vanishPairs(1, 0, 1, 2);
    players[0][0]->set_score(players[0][0]->get_score() + 2);
    EXPECT_FALSE(cb.isVanishable(1, 0, 1, 2));
    EXPECT_EQ(players[0][0]->get_score(), 2);

    // Player 2 flipped a card and invoked shuffle. It is asserted that the card now flipps back.
    EXPECT_EQ(players[1][0]->get_id(), "id2");
    EXPECT_EQ(players[1][0]->get_score(), 0);
    ASSERT_TRUE(cb.flipCard(2, 0));
    ASSERT_EQ(cards[11]->getIsFront(), 1);
    cb.shuffle();
    ASSERT_EQ(cards[11]->getIsFront(), 0);
    EXPECT_EQ(players[1][0]->get_score(), 0);
}

// The check that each instance game has a unique game ID must succeed
TEST_F(MemoryTest, GameIDCheck) {
    std::string err;
    CardBoard cb2;
    cb2.setup_game(err);
    std::vector<Card*> cards2;
    cards2 = cb2.getCards();
    std::vector<std::vector<Player*>> players2;
    players2.resize(2);
    players2[0].push_back(new Player("id1", "player_game_2_1", 0));
    players2[1].push_back(new Player("id2", "player_game_2_2", 0));

    EXPECT_NE(cb.get_id(), cb2.get_id());
}

// The check that multiple instance of games can be played must succeed.
TEST_F(MemoryTest, MultipleGameInstances) {
    // Setup
    std::string err;
    CardBoard cb2;
    cb2.setup_game(err);
    std::vector<Card*> cards2;
    cards2 = cb2.getCards();
    std::vector<std::vector<Player*>> players2;
    players2.resize(2);
    players2[0].push_back(new Player("id1", "player_game_2_1", 0));
    players2[1].push_back(new Player("id2", "player_game_2_2", 0));

    // Instance #1: cb1

    EXPECT_NE(cb.get_id(), cb2.get_id());

    // Player 1 successfully vanishes a pair of cards. Then it is Player 2's turn.
    EXPECT_EQ(players[0][0]->get_id(), "id1");
    EXPECT_EQ(players[0][0]->get_score(), 0);
    ASSERT_TRUE(cb.flipCard(1, 0));
    ASSERT_EQ(cards[6]->getIsFront(), 1);
    ASSERT_TRUE(cb.flipCard(1, 2));
    ASSERT_EQ(cards[3]->getIsFront(), 1);
    EXPECT_TRUE(cb.isVanishable(1, 0, 1, 2));
    cb.vanishPairs(1, 0, 1, 2);
    players[0][0]->set_score(players[0][0]->get_score() + 2);
    EXPECT_FALSE(cb.isVanishable(1, 0, 1, 2));
    EXPECT_EQ(players[0][0]->get_score(), 2);

    // Player 2 flipped a card and invoked shuffle. It is asserted that the card now flipps back.
    EXPECT_EQ(players[1][0]->get_id(), "id2");
    EXPECT_EQ(players[1][0]->get_score(), 0);
    ASSERT_TRUE(cb.flipCard(2, 0));
    ASSERT_EQ(cards[11]->getIsFront(), 1);
    cb.shuffle();
    ASSERT_EQ(cards[11]->getIsFront(), 0);
    EXPECT_EQ(players[1][0]->get_score(), 0);

    // Instance #2: cb2

    EXPECT_EQ(players2[0][0]->get_id(), "id1");
    EXPECT_EQ(players2[0][0]->get_score(), 0);

    // This time the check removes all card pairs to see if the all cards are actually being removed.
    // 1. Pick a card and flip it front.
    // 2. Check if the card is at the front face.
    // 3. Pick another card and flip it front.
    // 4. Check if this specific card is at the front face.
    // 5. Verify that two cards are vanishable.
    // 6. Vanish the card pair.
    // 7. Check if the cards are vanished.

    ASSERT_TRUE(cb2.flipCard(1, 0));
    ASSERT_EQ(cards2[6]->getIsFront(), 1);
    ASSERT_TRUE(cb2.flipCard(1, 2));
    ASSERT_EQ(cards2[3]->getIsFront(), 1);
    EXPECT_TRUE(cb2.isVanishable(1, 0, 1, 2));
    cb2.vanishPairs(1, 0, 1, 2);
    players2[0][0]->set_score(players2[0][0]->get_score() + 2);
    EXPECT_EQ(players2[0][0]->get_score(), 2);
    EXPECT_FALSE(cb2.isVanishable(1, 0, 1, 2));

    ASSERT_TRUE(cb2.flipCard(0, 2));
    ASSERT_EQ(cards2[0]->getIsFront(), 1);
    ASSERT_TRUE(cb2.flipCard(0, 3));
    ASSERT_EQ(cards2[9]->getIsFront(), 1);
    EXPECT_TRUE(cb2.isVanishable(0, 2, 0, 3));
    cb2.vanishPairs(0, 2, 0, 3);
    players2[0][0]->set_score(players2[0][0]->get_score() + 2);
    EXPECT_EQ(players2[0][0]->get_score(), 4);
    EXPECT_FALSE(cb2.isVanishable(0, 2, 0, 3));

    ASSERT_TRUE(cb2.flipCard(1, 3));
    ASSERT_EQ(cards2[1]->getIsFront(), 1);
    ASSERT_TRUE(cb2.flipCard(2, 3));
    ASSERT_EQ(cards2[4]->getIsFront(), 1);
    EXPECT_TRUE(cb2.isVanishable(1, 3, 2, 3));
    cb2.vanishPairs(1, 3, 2, 3);
    players2[0][0]->set_score(players2[0][0]->get_score() + 2);
    EXPECT_EQ(players2[0][0]->get_score(), 6);
    EXPECT_FALSE(cb2.isVanishable(1, 3, 2, 3));

    ASSERT_TRUE(cb2.flipCard(2, 1));
    ASSERT_EQ(cards2[7]->getIsFront(), 1);
    ASSERT_TRUE(cb2.flipCard(0, 1));
    ASSERT_EQ(cards2[10]->getIsFront(), 1);
    EXPECT_TRUE(cb2.isVanishable(2, 1, 0, 1));
    cb2.vanishPairs(2, 1, 0, 1);
    players2[0][0]->set_score(players2[0][0]->get_score() + 2);
    EXPECT_EQ(players2[0][0]->get_score(), 8);
    EXPECT_FALSE(cb2.isVanishable(2, 1, 0, 1));

    ASSERT_TRUE(cb2.flipCard(2, 2));
    ASSERT_EQ(cards2[2]->getIsFront(), 1);
    ASSERT_TRUE(cb2.flipCard(0, 0));
    ASSERT_EQ(cards2[5]->getIsFront(), 1);
    EXPECT_TRUE(cb2.isVanishable(2, 2, 0, 0));
    cb2.vanishPairs(2, 2, 0, 0);
    players2[0][0]->set_score(players2[0][0]->get_score() + 2);
    EXPECT_EQ(players2[0][0]->get_score(), 10);
    EXPECT_FALSE(cb2.isVanishable(2, 2, 0, 0));

    ASSERT_TRUE(cb2.flipCard(1, 1));
    ASSERT_EQ(cards2[8]->getIsFront(), 1);
    ASSERT_TRUE(cb2.flipCard(2, 0));
    ASSERT_EQ(cards2[11]->getIsFront(), 1);
    EXPECT_TRUE(cb2.isVanishable(1, 1, 2, 0));
    cb2.vanishPairs(1, 1, 2, 0);
    players2[0][0]->set_score(players2[0][0]->get_score() + 2);
    EXPECT_EQ(players2[0][0]->get_score(), 12);
    EXPECT_FALSE(cb2.isVanishable(1, 1, 2, 0));

    // The game should now end.
    EXPECT_FALSE(cb.processEndGame());
    EXPECT_TRUE(cb2.processEndGame());
}

// The check that shuffle works on multiple game instances must succeed. This checks if system memory would get scrambled under multiple game instances.
TEST_F(MemoryTest, MultipleInstancesShuffleCheck) {
    // Setup
    std::string err;
    CardBoard cb2;
    cb2.setup_game(err);
    std::vector<Card*> cards2;
    cards2 = cb2.getCards();
    std::vector<std::vector<Player*>> players2;
    players2.resize(2);
    players2[0].push_back(new Player("id1", "player_game_2_1", 0));
    players2[1].push_back(new Player("id2", "player_game_2_2", 0));

    // Game Instance #1: cb

    // Player 1
    EXPECT_EQ(players[0][0]->get_id(), "id1");
    EXPECT_EQ(players[0][0]->get_score(), 0);
    ASSERT_TRUE(cb.flipCard(1, 0));
    ASSERT_EQ(cards[6]->getIsFront(), 1);
    ASSERT_TRUE(cb.flipCard(1, 2));
    ASSERT_EQ(cards[3]->getIsFront(), 1);
    EXPECT_TRUE(cb.isVanishable(1, 0, 1, 2));
    cb.vanishPairs(1, 0, 1, 2);
    players[0][0]->set_score(players2[0][0]->get_score() + 2);
    EXPECT_EQ(players[0][0]->get_score(), 2);
    EXPECT_FALSE(cb.isVanishable(1, 0, 1, 2));

    // Player 2
    EXPECT_EQ(players[1][0]->get_id(), "id2");
    EXPECT_EQ(players[1][0]->get_score(), 0);
    ASSERT_TRUE(cb.flipCard(2, 0));
    ASSERT_EQ(cards[11]->getIsFront(), 1);
    cb.shuffle();
    ASSERT_EQ(cards[11]->getIsFront(), 0);
    EXPECT_EQ(players[1][0]->get_score(), 0);

    // Validate if cards are being shuffled.
    int initial_value = 1;
    int total_different_values = 0;
    for (int i = 0; i < 12; i++) {
        if (cards[i]->getValue() != initial_value) {
            total_different_values++;
        }
        initial_value++;
    }

    EXPECT_NE(total_different_values, 0);

    // Game Instance #2: cb2

    // Player 1
    EXPECT_EQ(players2[0][0]->get_id(), "id1");
    EXPECT_EQ(players2[0][0]->get_score(), 0);
    ASSERT_TRUE(cb2.flipCard(1, 0));
    ASSERT_EQ(cards2[6]->getIsFront(), 1);
    ASSERT_TRUE(cb2.flipCard(1, 1));
    ASSERT_EQ(cards2[8]->getIsFront(), 1);
    EXPECT_FALSE(cb2.isVanishable(1, 0, 1, 1));
    cards2[6]->flip();
    cards2[8]->flip();
    ASSERT_EQ(cards2[6]->getIsFront(), 0);
    ASSERT_EQ(cards2[8]->getIsFront(), 0);

    // Player 2
    EXPECT_EQ(players2[1][0]->get_id(), "id2");
    EXPECT_EQ(players2[1][0]->get_score(), 0);
    ASSERT_TRUE(cb2.flipCard(1, 0));
    ASSERT_EQ(cards2[6]->getIsFront(), 1);
    ASSERT_TRUE(cb2.flipCard(1, 2));
    ASSERT_EQ(cards2[3]->getIsFront(), 1);
    EXPECT_TRUE(cb2.isVanishable(1, 0, 1, 2));
    cb2.vanishPairs(1, 0, 1, 2);
    players2[1][0]->set_score(players2[1][0]->get_score() + 2);
    EXPECT_EQ(players2[1][0]->get_score(), 2);
    EXPECT_FALSE(cb2.isVanishable(1, 0, 1, 2));
    cb.shuffle();

    // Validate if cards are being shuffled.
    int initial_value2 = 1;
    int total_different_values2 = 0;
    for (int i = 0; i < 12; i++) {
        if (cards2[i]->getValue() != initial_value2) {
            total_different_values2++;
        }
        initial_value2++;
    }

    EXPECT_NE(total_different_values2, 0);
}
